local hook =  {}
local fish = {}

local dtotal = 0.0
local dpicked = 0.0

local score = 0

function love.load()
    hook.x = 130
    hook.y = 450
    hook.released = false
    hook.is_on_the_sea = false
    hook.is_returning = false
    hook.retries = 3

    fish.picked_up = false

    math.randomseed(os.time())

end

function love.update(dt)
    update_hook()

    if hook.is_on_the_sea then
        if not hook.is_returning then
            pick_fish(dt)
        end
    end
end

function love.draw()
    -- Player
    love.graphics.setColor(1,0,0)
    love.graphics.rectangle("fill", 80, 430, 50, 100)
    
    -- Sea
    love.graphics.setColor(0,0,1)
    love.graphics.line(0, 550, 800, 550)

    -- Ground
    love.graphics.setColor(0,1,0)
    love.graphics.rectangle("fill", 0, 530, 200, 100)

    -- Hook
    love.graphics.setColor(1,1,1)
    love.graphics.rectangle("fill", hook.x, hook.y, 10, 10)

    -- Picked up fish indicator
    if fish.picked_up then
        love.graphics.print("!", hook.x + 2.5, hook.y - 20)
    end

    -- Score
    love.graphics.print("Score: " .. score, 10, 10)
    love.graphics.print("Hooks: " .. hook.retries, 10, 25)
end

-- Player controls

function love.keypressed(key)
    if key == "space" then
        if not hook.released then
            release_hook()
        else
            get_hook()
        end
    end
end

-- Hook functions

function release_hook()
    hook.released = true
end

function get_hook()
    if hook.is_on_the_sea then
        hook.is_returning = true
    end
end

function update_hook()
    if hook.released then
        if not hook.is_on_the_sea then
            hook.x = hook.x + 3

            if hook.x < 300 then
                hook.y = hook.y - 3
            else
                hook.y = hook.y + 3
            end

            if hook.y >= 540 and not hook.is_returning then
                hook.y = 545
                hook.is_on_the_sea = true
            end
        end

        if hook.is_on_the_sea and hook.is_returning then
            if hook.x >= 200 then
                hook.x = hook.x - 3
            else
                reset_hook()
                if fish.picked_up then
                    fish.picked_up = false
                    score = score + 10
                end
            end
        end
    end
end

function reset_hook()
    hook.x = 130
    hook.y = 450
    hook.released = false
    hook.is_on_the_sea = false
    hook.is_returning = false
    dpicked = 0.0
end

-- Fishing functions

function pick_fish(dt)
    if not fish.picked_up then
        dtotal = dtotal + dt

        if dtotal >= 3.0 then
            if math.floor((math.random() * 10)) % 3 == 0 then
                dtotal = 0.0
                fish.picked_up = true
            end
        end
    end

    if fish.picked_up then
        dpicked = dpicked + dt
    else
        dpicked = 0.0
    end

    if dpicked >= 5.0 then
        fish.picked_up = false
        hook.retries = hook.retries - 1
        reset_hook()
    end
end